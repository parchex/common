<?php

namespace Spec\Parchex\Common;

class Content implements \Parchex\Common\ContentImmutable
{
    use \Parchex\Common\ContentImmutableTrait;
}

describe("Content with properties and with immutable structure (trait)", function () {

    describe("contracts", function () {
        it("serializes to json", function () {
            $content = new Content(["prop" => "value", "other" => "diff"]);

            expect(json_encode($content))->toBe('{"prop":"value","other":"diff"}');
        });

        it("access like a array", function () {
            $content = new Content(["prop" => "value"]);

            expect($content["prop"])->toBeTruthy();
        });
    });

    describe("access to properties values", function () {
        it("can access to properties", function () {
            $content = new Content(["prop" => "value"]);

            expect(isset($content["prop"]))->toBeTruthy();
            expect($content["prop"])->toBe("value");
        });

        it("cannot modified properties", function () {
            $content = new Content(["prop" => "value"]);

            expect(function () use ($content) {
                unset($content["prop"]);
            })->toThrow(new \LogicException());

            expect(function () use ($content) {
                $content["prop"] = "change";
            })->toThrow(new \LogicException());
        });

        it("copies adding new properties", function () {
            $content = new Content(["prop" => "value"]);

            $copy = $content->copyWith(["other" => "new"]);

            expect($copy)
                ->toBeAnInstanceOf(Content::class)
                ->not->toEqual($content)
                ->toContainKeys(['prop', 'other']);

            expect($content->jsonSerialize())->toEqual(["prop" => "value"]);
            expect($copy->jsonSerialize())->toEqual(["prop" => "value", "other" => "new"]);
        });
    });
});
