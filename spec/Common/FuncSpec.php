<?php

namespace Spec\Parchex\Common;

describe("Functional", function () {

    describe("Array map for any iterable values", function () {

        it("map with array", function () {
            $mapping = \Parchex\Common\Func::map("strtoupper", ["a", "b", "c"]);

            expect($mapping)->toEqual(["A", "B", "C"]);
        });

        it("map with iterable object", function () {
            $mapping = \Parchex\Common\Func::map("strtoupper", new \ArrayObject(["a", "b", "c"]));

            expect($mapping)->toEqual(["A", "B", "C"]);
        });
    });
});
