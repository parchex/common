<?php

namespace Spec\Parchex\Common;

use Assert\InvalidArgumentException;
use Doctrine\Common\Collections\Collection;
use Parchex\Common\ArrayCollection;
use Parchex\Common\Assertion;
use Parchex\Common\ValidationException;

describe("Array collection", function () {
    it("is a collection", function () {
        expect(new ArrayCollection())->toBeAnInstanceOf(Collection::class);
    });

    it("is a exception if given a type of elements all of them are not the same type", function () {
        expect(function () {
            new ArrayCollection(
                [new \ArrayObject(), new \DateTimeImmutable()],
                \ArrayObject::class
            );
        })->toThrow(new ValidationException("", Assertion::INVALID_INSTANCE_OF));
    });
});
