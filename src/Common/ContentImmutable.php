<?php declare(strict_types=1);

namespace Parchex\Common;

use ArrayAccess;
use JsonSerializable;

interface ContentImmutable extends ArrayAccess, JsonSerializable
{
}
