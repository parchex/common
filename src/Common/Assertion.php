<?php declare(strict_types=1);

namespace Parchex\Common;

use Assert\Assertion as BaseAssertion;
use Assert\AssertionFailedException;

/**
 * @method static bool allIsItemOfCollection(array $value, string $className, string|callable $message = null, string $propertyPath = null) Assert that item is instance of given class-name collection for all values.
 * @method static bool nullOrIsItemOfCollection(mixed|null $value, string $className, string|callable $message = null, string $propertyPath = null) Assert that item is instance of given class-name collection or that the value is null.
 */
class Assertion extends BaseAssertion
{
    /**
     * @var class-string
     */
    protected static $exceptionClass = ValidationException::class;

    /**
     * Assert that item is instance of given class-name collection.
     * {@inheritdoc}
     *
     * @param mixed $item
     * @param string $classCollection
     * @param string|callable|null $message
     *
     * @psalm-param class-string<object> $classCollection
     *
     * @throws AssertionFailedException
     */
    public static function isItemOfCollection(
        $item,
        string $classCollection,
        $message = null,
        ?string $propertyPath = null
    ): bool {
        $message = $message ?? "Element of type '%s' is not valid for collection of %s";

        return static::isInstanceOf(
            $item,
            $classCollection,
            $message,
            $propertyPath
        );
    }
}
