<?php declare(strict_types=1);

namespace Parchex\Common;

/**
 * Wrapper from Doctrine collection
 *
 * {@inheritdoc}
 */
class ArrayCollection extends \Doctrine\Common\Collections\ArrayCollection
{
    /**
     * {@inheritdoc}
     *
     * @param array<object> $items
     */
    public function __construct(array $items = [], string $classType = '')
    {
        if ($classType !== '') {
            Assertion::allIsItemOfCollection($items, $classType);
        }

        parent::__construct($items);
    }
}
