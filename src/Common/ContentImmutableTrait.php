<?php declare(strict_types=1);

namespace Parchex\Common;

use LogicException;

trait ContentImmutableTrait
{
    /**
     * @var array<mixed>
     */
    protected $prop;

    /**
     * {@inheritdoc}
     *
     * @param array<mixed> $prop
     */
    public function __construct(array $prop = [])
    {
        $this->prop = $prop;
    }

    /**
     * @return array<mixed>
     */
    public function jsonSerialize()
    {
        return $this->prop;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        throw new LogicException("Cannot modified properties (${offset} => ${value})");
    }

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->prop[$offset]);
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset): void
    {
        throw new LogicException("Cannot modified properties (${offset})");
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->prop[$offset] ?? null;
    }

    /**
     * @param array<mixed> $values
     *
     * @return static
     */
    public function copyWith(array $values)
    {
        $copy = clone $this;
        $copy->prop = array_replace_recursive($this->prop, $values) ?? $this->prop;

        return $copy;
    }
}
