<?php

namespace Spec\Parchex\Common;

use Parchex\Common\DateDifference;
use Parchex\Common\DateTime;

describe("Date Time ", function () {

    beforeAll(function () {
        date_default_timezone_set('UTC');
    });

    describe("create date", function () {
        it("gives current date time", function () {
            $now = (new \DateTime())->format(DateTime::FORMAT);

            expect((string)DateTime::now())->not->toBeLessThan($now);
        });

        it("gives tomorrow date time", function () {
            $tomorrow = (new \DateTime("tomorrow"))
                ->format(DateTime::FORMAT);

            expect((string)DateTime::tomorrow())->not->toBeLessThan($tomorrow);
        });

        it("creates from a date string", function () {
            expect(DateTime::fromString("2017-12-15T10:20:30+01:00"))->toBeAnInstanceOf(DateTime::class);
        });
    });

    describe("conversion", function () {
        it("parses to string", function () {
            $strDate = "2017-12-15T10:20:30+01:00";
            $dateTime = DateTime::fromString($strDate);

            expect((string)$dateTime)->toBe($strDate);
        });

        it("parses to a json format", function () {
            $strDate = "2017-12-15T10:20:30+01:00";
            $dateTime = DateTime::fromString($strDate);

            expect(json_encode($dateTime))->toBe('"'.$strDate.'"');
        });
    });

    describe("modification", function () {
        it("adds a interval of time", function () {
            $dateTime = DateTime::fromString("2012-02-25T12:00:00+02:00");
            $dateTimeAdded = $dateTime->add("P20DT12H30M45S");

            expect((string)$dateTimeAdded)->toBe('2012-03-17T00:30:45+02:00');
        });

        it("subtracts a interval of time", function () {
            $dateTime = DateTime::fromString("2012-02-25T12:00:00+02:00");
            $dateTimeSubtracted = $dateTime->sub("P20DT12H30M45S");

            expect((string)$dateTimeSubtracted)->toBe('2012-02-04T23:29:15+02:00');
        });

        it("is immutable when applies a interval", function () {
            $dateTime = DateTime::now();
            $dateAdded = $dateTime->add("P1Y");
            $dateSubtract = $dateTime->sub("P1Y");

            expect($dateTime)->not->toBe($dateAdded)->not->toBe($dateSubtract);
        });
    });

    describe("comparative", function () {
        it("is equal when has same value", function () {
            $dateTime = DateTime::fromString('2013-04-22T22:33:00');
            $oneDate = DateTime::fromString('22-04-2013 22:33');
            $otherDate = DateTime::fromString('12/12/2012 12:12:12');


            expect($dateTime->equals($oneDate))->toBeTruthy();
            expect($dateTime->equals($otherDate))->toBeFalsy();
        });

        it("gives date interval difference with other date", function () {
            $oneDate = DateTime::fromString("2012-02-04T23:29:15+02:00");
            $otherDate = DateTime::fromString("2012-02-25T12:00:00+02:00");

            $diffInterval = $oneDate->diff($otherDate)->format('P%yY%mM%dDT%hH%iM%sS');

            expect($diffInterval)->toBe("P0Y0M20DT12H30M45S");
        });
    });

    describe("native", function () {
        it("provides original date time from PHP", function () {
            $dateTime = DateTime::now();
            expect($dateTime->toNative())
                ->toBeAnInstanceOf(\DateTimeImmutable::class);
        });
    });

    describe("difference between two dates", function () {
        it("is exception when interval is not between two dates", function () {
            $notIntervalDifference = function () {
                new DateDifference(
                    \DateInterval::createFromDateString(" 1 day")
                );
            };

            expect($notIntervalDifference)->toThrow(new \LogicException());
        });

        it("formats with complete info about interval difference", function () {
            $difference = DateTime::now()->diff(DateTime::tomorrow());

            expect($difference->format())
                ->toMatch('/^P\d+Y\d+M\d+DT\d+H\d+M\d+S$/');
        });

        it("gives difference in days and seconds", function () {
            $oneDate = DateTime::fromString("2012-02-04T23:29:15+02:00");
            $otherDate = DateTime::fromString("2012-02-25T12:00:00+02:00");

            $difference = $oneDate->diff($otherDate);

            expect($difference->days())->toBe(20);
            expect($difference->seconds())->toBe(12 * 3600 + 30 * 60 + 45);
            expect($difference->microseconds())->toBeCloseTo($difference->seconds());
        });
    });
});
