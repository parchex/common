### Components config selection
##
# Docker (choose one... )
#  values: docker-compose | docker-engine (default)
MAKEUP_HAS += docker-engine
##
# Merge release (choose one... )
#  values:  git-flow | git-trunk (default)
MAKEUP_HAS += git-trunk
##
# PHP options...
#  values: php-composer php-build
#          php-qa
#          php-doctrine php-symfony
#          php-tests-xunit php-tests-mutant php-tests-rspec php-tests-bdd
MAKEUP_HAS += php-composer php-tests-rspec php-qa

### Tag rules to update files with new version
TAG_APP_FILES += dot.env
dot.env.tag: TAG_REG_EXP=/APP_VERSION=.*/APP_VERSION=${TAG}/

###
include make/MakeUp.mk
