<?php declare(strict_types=1);

namespace Parchex\Common;

use DateInterval;
use LogicException;

class DateDifference
{
    public const DIFF_INTERVAL = 'P%yY%mM%dDT%hH%iM%sS';
    /**
     * @var DateInterval
     */
    private $interval;
    /**
     * @var string
     */
    private $diffFormat = '';
    /**
     * @var int
     */
    private $days = 0;
    /**
     * @var int
     */
    private $seconds = 0;
    /**
     * @var float
     */
    private $microseconds = 0.0;

    public function __construct(DateInterval $diffInterval)
    {
        if ($diffInterval->days === false) {
            throw new LogicException(
                'Only can be date intervals from a diff between dates'
            );
        }

        $this->interval = $diffInterval;

        $this->diffFormat = $diffInterval->format(DateDifference::DIFF_INTERVAL);

        $this->days = $diffInterval->days;
        $this->seconds = $diffInterval->h * 60 * 60
            + $diffInterval->i * 60
            + $diffInterval->s;
        $this->microseconds = $diffInterval->f;
    }

    public function format(): string
    {
        return $this->diffFormat;
    }

    public function days(): int
    {
        return $this->days;
    }

    public function seconds(): int
    {
        return $this->seconds;
    }

    public function microseconds(): float
    {
        return (float) $this->seconds() + $this->microseconds;
    }

    public function toNative(): DateInterval
    {
        return $this->interval;
    }
}
