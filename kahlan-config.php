<?php

function with_provided_data_it($message, $provider, $closure, $timeout = null, $scope = 'normal')
{
    $data = is_callable($provider) ? $provider() : $provider;
    foreach ($data as $title => $values) {
        \Kahlan\Suite::current()->it(
            \Kahlan\Util\Text::insert($message, $values) . ' with data set "' . $title . '"',
            function () use ($closure, $values) {
                call_user_func_array($closure, $values);
            },
            $timeout,
            $scope
        );
    }
}
