<?php declare(strict_types=1);

namespace Parchex\Common;

use Assert\AssertionFailedException;
use DomainException;

class ValidationException extends DomainException implements AssertionFailedException
{
    /**
     * @var string|null
     */
    private $propertyPath;
    /**
     * @var mixed
     */
    private $value;
    /**
     * @var array<string>
     */
    private $constraints;

    /**
     * {@inheritdoc}
     *
     * @param array<string> $constraints
     * @param mixed $value
     */
    public function __construct(
        string $message = '',
        int $code = 0,
        string $propertyPath = null,
        $value = null,
        array $constraints = []
    ) {
        parent::__construct($message, $code);

        $this->propertyPath = $propertyPath;
        $this->value = $value;
        $this->constraints = $constraints;
    }

    /**
     * User controlled way to define a sub-property causing
     * the failure of a currently asserted objects.
     *
     * Useful to transport information about the nature of the error
     * back to higher layers.
     */
    public function getPropertyPath(): ?string
    {
        return $this->propertyPath;
    }

    /**
     * Get the value that caused the assertion to fail.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get the constraints that applied to the failed assertion.
     *
     * @return array<string>
     */
    public function getConstraints(): array
    {
        return $this->constraints;
    }
}
