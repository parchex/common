<?php declare(strict_types=1);

namespace Parchex\Common;

use DateInterval;
use DateTimeImmutable;
use JsonSerializable;

/**
 * Specific immutable DateTime
 */
final class DateTime implements JsonSerializable
{
    public const FORMAT = \DateTime::ATOM;
    /**
     * @var DateTimeImmutable
     */
    private $dateTime;

    private function __construct(DateTimeImmutable $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * Actual time moment
     */
    public static function now(): self
    {
        return new self(new DateTimeImmutable('now'));
    }

    /**
     * Tomorrow datetime
     */
    public static function tomorrow(): self
    {
        return new self(new DateTimeImmutable('tomorrow'));
    }

    /**
     * Create a instance from a date string format valid
     *
     * About formats...
     *
     * http://php.net/manual/en/datetime.formats.php
     *
     * {@inheritdoc}
     */
    public static function fromString(string $dateTimeString): self
    {
        return new self(new DateTimeImmutable($dateTimeString));
    }

    /**
     * Compare with other date if has the same date
     *
     * {@inheritdoc}
     */
    public function equals(self $dateTime): bool
    {
        return $this->dateTime == $dateTime->toNative();
    }

    /**
     * @return DateTimeImmutable PHP date time
     */
    public function toNative(): DateTimeImmutable
    {
        return $this->dateTime;
    }

    /**
     * Increment interval date/time with formats like...
     *
     * http://php.net/manual/en/dateinterval.construct.php
     *
     * Example...
     *     $date->add('P2DT12H');
     *     $date->add('P1Y6M14DT13H20M47S');
     */
    public function add(string $interval): self
    {
        $dateTime = $this->dateTime->add(new DateInterval($interval));

        return new self($dateTime);
    }

    /**
     * Subtracts interval date/time with formats like...
     *
     * http://php.net/manual/en/dateinterval.construct.php
     *
     * Example...
     *     $date->sub('P2DT12H');
     *     $date->sub('P1Y6M14DT13H20M47S');
     *
     * {@inheritdoc}
     */
    public function sub(string $interval): self
    {
        return new self(
            $this->dateTime->sub(new DateInterval($interval))
        );
    }

    /**
     * Calculate time difference with other date
     *
     * {@inheritdoc}
     */
    public function diff(self $dateTime): DateDifference
    {
        return new DateDifference($this->dateTime->diff($dateTime->dateTime));
    }

    public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    /**
     * Transform date time to literal string format
     *
     * @see DateTime::FORMAT
     *
     * @return string
     */
    public function __toString()
    {
        return $this->dateTime->format(self::FORMAT);
    }
}
