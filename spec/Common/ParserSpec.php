<?php

namespace Spec\Parchex\Common;

use Parchex\Common\Parser;

describe("Parser tools", function () {

    describe("Parser for config sort param", function () {

        it("sets default sort to ascend", function () {
            $sort = \Parchex\Common\Parser:: sortType("field");

            expect($sort)->toEqual(["field" => "ASC"]);
        });
        it("parses param to set order", function () {
            $sort = \Parchex\Common\Parser:: sortType("field|ASC");

            expect($sort)->toEqual(["field" => "ASC"]);
        });
        it("parses multiple options", function () {
            $sort = \Parchex\Common\Parser:: sortType("field|ASC,other|DESC");

            expect($sort)->toEqual(["field" => "ASC", "other" => "DESC"]);
        });

        given("sortParams", function () {
            return [
                "a field" => [
                    "a_field",
                    ['a_field' => 'ASC']
                ],
                "different fields and orders" => [
                    "a_field|ASC,other_field|DESC",
                    [
                        'a_field'     => 'ASC',
                        'other_field' => 'DESC'
                    ]
                ],
                "orders in lowercase" => [
                    "other_field|desc,a_field|asc",
                    [
                        'other_field' => 'DESC',
                        'a_field'     => 'ASC'
                    ]
                ]
            ];
        });

        with_provided_data_it(
            "parsers param string into a array config",
            $this->sortParams,
            function ($param, $expected) {
                expect(Parser::sortType($param))->toBe($expected);
            }
        );
    });
});
