<?php declare(strict_types=1);

namespace Parchex\Common;

class Func
{
    /**
     * Wrapper to mapping elements of arrays or objects iterables
     *
     * {@inheritdoc}
     *
     * @param array<mixed> $traversable
     *
     * @return array<mixed>
     */
    public static function map(callable $callable, iterable $traversable): array
    {
        if (is_array($traversable)) {
            return array_map($callable, $traversable);
        }

        $mapping = [];
        foreach ($traversable as $item) {
            $mapping[] = call_user_func($callable, $item);
        }

        return $mapping;
    }

    /**
     * Wrapper to filter elements of arrays or objects iterables
     *
     * {@inheritdoc}
     *
     * @param array<mixed> $traversable
     *
     * @return array<mixed>
     */
    public static function filter(callable $callable, iterable $traversable): array
    {
        if (is_array($traversable)) {
            return array_filter($traversable, $callable);
        }

        $filtered = [];
        foreach ($traversable as $item) {
            if ($callable($item)) {
                $filtered[] = $item;
            }
        }

        return $filtered;
    }
}
