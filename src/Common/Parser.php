<?php declare(strict_types=1);

namespace Parchex\Common;

class Parser
{
    /**
     * Parse a param type 'name|ASC,other|desc' to
     * <code>
     * [
     *   'name' => 'ASC',
     *   'other' => 'DESC'
     * ]
     * </code>
     *
     * {@inheritdoc}
     *
     * @return array<object>
     */
    public static function sortType(string $param): array
    {
        if ($param === '') {
            return [];
        }

        return array_reduce(
            array_map(
                static function ($field): array {
                    $field = explode('|', $field);
                    $orderBy = $field[0];
                    $order = $field[1] ?? 'ASC';

                    return [strtolower($orderBy), strtoupper($order)];
                },
                explode(',', $param)
            ),
            static function ($carry, $sortBy) {
                $carry[$sortBy[0]] = $sortBy[1];

                return $carry;
            },
            []
        );
    }
}
