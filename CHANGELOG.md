# Changelog


## v0.1.3 - 2021-10-14

* downgrade assert library

## v0.1.1 - 2021-10-09

* update tools and config

## v0.1.0 - 2020-11-17

* Package with common utils to use in development
  * Collection
  * Asserts
  * Content Inmutable
  * Date and time
  * Enumerates
  * map/reduce functionality
